# 7. Functors and monoids

- [IO](#io)
- [Arrow type constructor](#arrow-type-constructor)
- [Lifting functions](#lifting-functions)
- [Functor laws](#functor-laws)
- [Applicative functors](#applicative-functors)
- [Using `newtype` with functors](#using-newtype-with-functors)
- [Monoids](#monoids)

The `Functor` typeclass defines the behaviour of types that can be mapped over:

```haskell
class Functor f where
    fmap :: (a -> b) -> f a -> f b
```

So all `Functors` must implement a `fmap` function, and this function takes a function and a functor and return a new functor. The interesting thing here is that `f` is really a type constructor, rather than a concrete type. This is important because we want this interface to be implemented by generic types. For instance, lists are functors, and `map` is the `fmap` functor implementation for lists: in fact, lists are type constructors, because we can have `[Int]`, `[Char]`, etc., and `map` should be defined only once for all of them:

```haskell
instance Functor [] where
    fmap = map
```

here we didn't pass a concrete type, like `[Int]` as type parameter, because the type parameter should be a type constructor, like `[]` is.

Also `Maybe` is a functor:

```haskell
instance Functor Maybe where
    fmap f (Just x) = Just (f x)
    fmap f Nothing = Nothing
```

In the functor definition the constructor should take only one parameter: `fmap :: (a -> b) -> f a -> f b`, the type constructor `f` only takes one argument, `a` or `b`. What if we want to define a functor from a type constructor that takes more than one parameter, like `Either a b`? We partially apply the type constructor:

```haskell
instance Functor (Either a) where
    fmap f (Right x) = Right (f x)
    fmap f (Left x) = Left x
```

Here, an `Either` can contain values of different types, for instance either a string or a number. However, the function `f` only takes one argument of one type, for example a string, so we can't assume that `f` can be called on both `Right x` and `Left x`, and we must apply `f` to only one of them: the second (right), because the first is fixed since we are using a partial application of `Either`.

In general, a functor is a type that represents a *computational context*, meaning that a function is applied to a special context. For example, in the case of monads, a function is used in a context where the computation can have a result, or not; in the case of lists, the context is such that the computation is applied to a value, but there may be more values, and so on.

## IO
The `IO` type constructor is also a functor:

```haskell
instance Functor IO where
    fmap f action = do
        result <- action
        return (f result)
```

so mapping a function `f` over an action has the effect of executing that action, extracting its production, and re-incapsulating it inside a new action, with `return`, that is returned. Take a look at this:

```haskell
main = do line <- getLine
          let line' = reverse line
          putStrLn $ "You said " ++ line' ++ " backwards!"
          putStrLn $ "Yes, you really said " ++ line' ++ " backwards!"
```

this can be rewritten as:

```haskell
main = do line <- fmap reverse getLine
          putStrLn $ "You said " ++ line ++ " backwards!"
          putStrLn $ "Yes, you really said " ++ line ++ " backwards!"
```

so `fmap reverse getLine` means mapping the function `reverse` on the functor returned by `getLine`: what we get back is another functor, and in this case an IO, incapsulating the result of that function application, that is the string the user gave us, backwards. So `fmap` is quite handy when we have to apply a function to the contents of an IO: instead of using a temporary identifier to extract that value, and then another identifier to bind the result of the function application to the first identifier, we can just use `fmap` to apply the function to the action, and bind the result to only one identifier.

## Arrow type constructor
We are used to define lambda functions as:

```haskell
\x -> f x
```

where this is an anonymous function taking an argument `x`, and returning the result of `f x` (or, to be more precise, returning the expression `f x`), which still hasn't been evaluated because of greedy evaluation. What we still didn't mentioned is that the operator `(->)` is a function as well, as obvious it may be. And not only it's a function: it's a functor, too, and like `Either` it takes two parameters (being an infix operator):

```haskell
instance Functor ((->) r) where
    fmap f g = (\x -> f (g x))
```

Our functor is, more precisely, `(-> r)`, because since functors must be type constructors taking only one parameter, to get there we have to partially apply our `(->)`. So our `fmap` gets the signature `fmap :: (a -> b) -> ((->) r a) -> ((->) r b)`, which can be rewritten as `fmap :: (a -> b) -> (r -> a) -> (r -> b)`. So this functor is a type constructor that takes a function from `a` to `b` and another function from `r` to `a`, and returns a third function from `r` to `b`; so, it's doing function composition:

```haskell
instance Functor ((->) r) where
    fmap = (.)
```

it can be rewritten like this because it's behaviour is precisely that of the function composition operator `(.)`.

## Lifting functions
Given that all Haskell functions ultimately take only one parameter, possibly returning a curried function, we can rewrite `fmap` signature as `(Functor f) => fmap :: (a -> b) -> (f a -> f b)`, so `fmap` is a function that takes a function `a -> b` and returns another (curried) function `f a -> f b`, which in turn takes a functor of type `a`, `f a`, and returns another functor of type `b`, `f b`. So from a function `a -> b` we got to a function `f a -> f b`: this is called *lifting* a function, as if we were moving it to a superior level, the functor one. For example:

```
ghci> :t (*2)
(*2) :: Num a => a -> a
ghci> :t fmap (*2)
fmap (*2) :: (Functor f, Num b) => f b -> f b
```

Here `fmap (*2)` is a function that takes a functor on `Num` and returns another functor on `Num`, because obviously `(*3)` takes a `Num` and returns another `Num`. Consider this:

```
ghci> let f = fmap (++ ['a']) in f getline
test
"testa"
```

Here we are defining our curried `fmap` as `fmap (++ ['a'])`: this has type `Functor f => f [Char] -> f [Char]`, so it's a function that takes a functor on a string, and returns another functor on a string. A functor on strings that we already know is obviously `IO String`, so in `f getline` we are using `getline` to produce our `IO String`, which incapsulates the string `test` we passed on the console. This IO is then passed to `f`, which means `fmap`, which uses its implementation for managing IOs. `fmap` for IO calls the argument function `(++ ['a'])` on the value incapsulated in the IO, extracted with `return`, and this means that the string `['a']` is appended to the production of the IO.

Let's see a case where `f` returns a different type of functor:

```
ghci> :t fmap (replicate 3)
fmap (replicate 3) :: (Functor f) => f a -> f [a]
ghci> let f = fmap (replicate 3) in f "hello"
["hhh", "eee", "lll", "lll", "ooo"]
```

Here `replicate 3` takes a value and returns a list containing that value replicated three times. In `f "hello"` we are passing to `f` the functor `[Char]` (which means the type constructor `[]` applied to `Char`). The implementation of `fmap` for lists states that the argument function is called on each element of the list. In this case the argument function is `replicate 3`, which is therefor called on each element of the list `"hello"`.

## Functor laws
We've seen that functors implement the `fmap` function, which allow them to be mapped over by some other function. Actually, this is not quite true, because the definition of `fmap` only specify that it should take a function, and nothing else: one is free of implementing a functor whose `fmap` doesn't map a function over it.

To have real functors, the functor concept must be enforced by the developer creating new functors. This concept is defined by the two laws of functors.

The first law of functors states that if we map the `id` function over a functor, the functor we get back should be the same of the original functor. This means that functors implement the identity principle. For instance:

```
ghci> fmap id [3]
[3]
```

This could seem obvious because the identity function `id` always returns its parameter: `id 3` is `3`, `id [4]` is `[4]`, etc. It seems obvious that also `fmap id [3]` should return `[3]`. However, one could implement `fmap` so that this isn't true anymore:

```haskell
instance Functor Maybe where
    fmap f (Just x) = Just (f x)
    fmap f Nothing = Nothing
```

Here, if `f` is `id`, we have `Just (id x)` which is `Just x`: the same as the parameter, and obviously `Nothing` is the same as the parameter `Nothing`. But if we defined this functor so that `fmap f Nothing = 3`, for some reason, suddenly  `fmap id Nothing` wouldn't be `Nothing` anymore, it would be `3`, breaking the first functor law.

The second functor law states that composing two functions and then mapping the resulting function over a functor should be the same as mapping one funciton over the functor, and then mapping the other one. So the functor should be associative: `fmap (f . g) F = fmap f . (fmap g F)`.

Functors that obey these two laws assure us that using `fmap` over them has a consistent behaviour, and this can be described as mapping over a collection of values incapsulated inside the functor.

Consider this example:

```haskell
data CMaybe a = CNothing | CJust Int a deriving (Show)
```

this is a special version of `Maybe` where the `Just` part holds both a generic value, and a numeric counter.

```haskell
instance Functor CMaybe where
    fmap f CNothing = CNothing
    fmap f (CJust counter x) = CJust (counter + 1) (fx)
```

The idea here is that each time a function is mapped over this functor, the counter is incremented.

```
ghci> fmap (++"ha") (CJust 0 "ho")
CJust 1 "hoha"
ghci> fmap (++"he") (fmap (++"ha") (CJust 0 "ho"))
CJust 2 "hohahe"
ghci> fmap (++"blah") CNothing
CNothing
```

Let's check if the first functor law is respected:

```
ghci> fmap id (CJust 0 "haha")
CJust 1 "haha"
ghci> id (CJust 0 "haha")
CJust 0 "haha"
```

We already have a failure here, because in the first example, mapping `id` over `CJust 0 "haha"` should return the same `CJust 0 "haha"`, but it doesn't, because the counter is incremented. So, even if `CMaybe` implements the typeclass `Functor`, it's not a real functor, as far as the conceptual notion of functors is concerned. Furthermore, it's easy to imagine how also the second functor low would fail here: the second law states that it shouldn't make any difference if we first do the function composition, and then call `fmap`, or first we call `fmap` on the functions, and then compose the results, but `CMaybe` makes a difference, because it counts how many times `fmap` is called.

## Applicative functors
We've seen that we can map curried functions over functors:

```
ghci> fmap (* 3) [1, 2, 3]
[3,6,9]
```

Here we are using `Num a => fmap (a -> a) -> [a] -> [a]`: we said that `fmap` takes a function that takes only one parameter, but using curried function we can make `fmap` work also with functions that take more than one parameter.

Now, what happens if we try to map a function that takes more than one parameter on a functor?

```
ghci> :t fmap (++) (Just "hey")
fmap (++) (Just "hey") :: Maybe ([Char] -> [Char])
```

Turns out we can do that, and what we get back is a functor applied over a curried function, resulting from partially applying the original function to the elements contained inside the functor. In this case, we are mapping `(++)`, which is a `[Char] -> [Char] -> [Char]` over an instance of `Maybe [Char]`, so the original function gets partially applied to the `[Char]` found inside the functor, and so what remains inside the functor is a partially applied function `[Char] -> [Char]`. Consider that:

```
ghci> :t fmap (++ "a") (Just "hey")
fmap (++ "a") (Just "hey") :: Maybe [Char]
ghci> fmap (++ "a") (Just "hey")
Just "heya"
```

so, when we normally use functors with one-argument functions, we get back the same kind of functor with a new content, which is the result of calling the given function over the given functor. So, it's perfectly logical that if we pass a two-argument function to `fmap` we get back a functor with a partially applied function in it.

Given a functor containing a partially applied function, we can map still another function over this functor, and this would mean mapping a composite function over the functor:

```
ghci> let a = fmap (*) [1, 2, 3, 4]
ghci> :t a
a :: [Integer -> Integer]
ghci> fmap (\f -> f 9) a
[9,18,27,36]
```

So `a` is a functor containing the `*` function partially applied to the elements of the list `[1, 2, 3, 4]`, so it's like having a list of partially applied functions like `[(* 1), (* 2), (* 3), (* 4)]`. Next we map `\f -> f 9` over it: here the parameter `f` is given the content of the functor, but in this case the content of the functor is another function, so `f` gets a function as a value. In particular, `f` assumes the values `(* 1)`, `(* 2)`, and so on. Then, the function we are mapping over this list returns `f 9`, which means the result of the application of `f` to `9`: `[(* 1 9), (* 2 9), (* 3 9), (* 4 9)]`, which is obviously `[9, 18, 27, 36]`.

Now, suppose that, instead of having a functor and a function to map over it, we have a functor and a function contained inside a functor. We can't map the function hidden inside the functor, over the other functor, because this is not how `fmap` works: `fmap` takes a normal function and a functor, not two functors. The solution to this problem is given by the `Applicative` typeclass, found inside the `Control.Applicative` module, which defines the methods `pure` and `<*>`:

```haskell
class (Functor f) => Applicative f where
    pure :: a -> f a
    (<*>) :: f (a -> b) -> f a -> f b
```

So an `Applicative` type should also be a `Functor` first (and so accepts also `fmap`). The `pure` function takes a value of any kind, `a`, and returns a functor with that value inside it, `f a`. For instance, if `f` was `Maybe`, and `a` was `Char`, `pure` would have returned a value of type `Maybe Char `, just like what happens when we do `Just 'a'`:

```
ghci> :t Just 'a'
Just 'a' :: Maybe Char
```

that takes `a` and puts it inside the `Maybe` functor.

The `<*>` function takes a functor containing a function `f (a -> b)` and another functor, and returns still another functor where the function contained inside the first functor has been applied to the contents of the second functor: this is precisely what we couldn't do with `fmap`!

Turns out that `Maybe` is also an applicative functor:

```haskell
instance Applicative Maybe where
    pure = Just
    Nothing <*> _ = Nothing
    (Just f) <*> something = fmap f something
```

As we've seen before, the behaviour of `Just` is just what we need for implementing `pure` for `Maybe`s. Next, `<*>` has a different behaviour according to whether the concrete type is `Nothing` or `Just`. In the first case, mapping anything over `Nothing` should still return `Nothing`. In the second case, mapping a functor `something` over a `Just` containing a function `f` should return `f` applied over the functor `something`, and we do this using the good old `fmap` function, that now can work because we just extracted the `f` function out of its container functor via pattern matching.

```
ghci> pure (+3) <*> Just 10
Just 13
ghci> Just (++"hahah") <*> Nothing
Nothing
```

Applicative functors allows also to map a function over several functors:

```
ghci> pure (+) <*> Just 3 <*> Just 5
Just 8
ghci> pure (+) <*> Just 3 <*> Nothing
Nothing
```

Here `<*>` is left-associative, so it's like we've written `(pure (+) <*> Just 3) <*> Just 5`, which is reduced to `Just (+ 3) <*> Just 5`, which is finally reduced to `Just 8`.

Let's say we have a normal function, taking values that aren't necessarily wrapped in functors, like `(*)`, but the values we want to pass to that function are wrapped in functors, like `[1]` and `[2]`. Obviously we can't do `[1] + [2]` and expect to get `[3]` as a result, and we couldn't even use `fmap`, because we don't have a single functor containing both values (it would have worked if we have had `fmap (\(x,y) -> x + y) [(1,2)]`). Applicative functors, however, let we take functions that expects regular parameters, and use them with functors, like `pure (+) <*> [1] <*> [2]` that returns `[3]`.

Furthermore, turns out that `pure f <*> x` equals `fmap f x`, so `pure (+) <*> [1] <*> [2]` can be written as `fmap (+) [1] <*> [2]`, and still yields `[3]`. For this reason, `Control.Applicative` exports the `<$>` function, which is the same as `fmap`, used as an infix operator:

```haskell
(<$>) :: (Functor f) => (a -> b) -> f a -> f b
f <$> x = fmap f x
```

so that we can write `f <$> x <*> y <*> z` , which means calling `f` passing to it `x`, `y`, and `z` as functors, and getting the result inside a functor as well.

We used also lists in our previous examples, because they are applicative functors too:

```haskell
instance Applicative [] where
    pure x = [x]
    fs <*> xs = [f x | f <- fs, x <- xs]
```

using list comprehension, `fs <*> xs` is the set of all function applications `f x` where `f` is `fs` and `x` is `xs`, meaning that we apply every possible function from the `fs` list, to every possible value from the `xs` list. For example, to see all possible products from two lists:

```
ghci> (*) <$> [2,5,10] <*> [8,10,11]
[16,20,22,40,50,55,80,100,110]
```

## Using `newtype` with functors
It's easy to make `Maybe` an instance of functor because a `Maybe` can either be a `Nothing` or a `Just`, it doesn't contain composite data, so we can just map the `f` of

```haskell
class Functor f where
    fmap :: (a -> b) -> f a -> f b
```

to the whole `Maybe`. But what if we wanted to make `(a,b)` an instance of `Functor`, so that when using `fmap` the function is applied only to the first element of the tuple, so that `fmap (+3) (1,1)` would result in `(4,1)`? If we defined something like `instance Functor (a,b) where` there would be no way to access only the parameter `a`. Anyway, we can do that if we wrap `(a,b)` inside a new type constructor exposing both elements `a` and `b`:

```haskell
newtype Pair b a = Pair { getPair :: (a,b) }
instance Functor (Pair c) where
    fmap f (Pair (x,y)) = Pair (f x, y)
```

```
ghci> getPair $ fmap (*100) (Pair (2,3))
(200,3)
ghci> getPair $ fmap reverse (Pair ("london calling", 3))
("gnillac nodnol", 3)
```

## Monoids
Several data types support associative functions, and values representing identities for those functions. For instance, the `*` function is associative, and `1` is the identity for the multiplication; similarly, `++` is associative, and the empty list is its identity value.

Types that support an associative function for which it exists an identity value, are called *monoids*, from `Data.Monoid`:

```haskell
class Monoid m where
    mempty :: m
    mappend :: m -> m -> m
    mconcat :: [m] -> m
    mconcat = foldr mappend mempty
```

If we recall the `Functor` definition:

```haskell
class Functor f where
    fmap :: (a -> b) -> f a -> f b
```

The `f` here is a type constructor, like `Maybe`, `IO` or `[]`, because it takes type parameters in `f a` and `f b`. The `m` in the `Monoid` definition, instead, doesn't take type parameters, and so must be a concrete type, like `Char` or `Maybe Float`.

The `mempty` polymorphic constant represents the identity value for the monoid. `mappend` takes two values of one type, and returns another value of the same kind: it's the equivalent of `*` or `++`. `mconcat` takes a list of monoid values and reduces them to a single value calling `mappend` to each element of the list, starting from the identity value.

Like `Functor`s, `Monoid`s must respect some laws:

- ``mempty `mappend` x = x``
- `` x `mappend` mempty = x``
- ``(x `mappend` y) `mappend` z = x `mappend` (y `mappend` z)``

simply stating that `mappend` should be associative, and `mempty` should be the identity value.

Monoids are useful for make folds over data structures. Data structure that work well with folds implement the `Foldable` type class, from `Data.Foldable`: like `Functor` is for things that can be mapped over, `Foldable` is for things that can be folded up.