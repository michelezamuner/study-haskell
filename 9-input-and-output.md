# 9. Input and output

- [Impure values](#impure-values)
- [Action binding](#action-binding)
- [Nested actions](#nested-actions)
- [*Let* bindings](#let-bindings)
- [Using `return`](#using-return)
- [Exceptions](#exceptions)

A Haskell program is really just a single function, that has to be called `main`, and interacts with the outside world, while typically doing also other computations in the meanwhile. The most simple complete Haskell program could be something like this:

```haskell
main = putStrLn "hello, world"
```

If we put this line inside a file named, for instance, `helloworld.hs`, we can compile it into an executable binary with:

```
$ ghc --make helloworld
```

What this program does is, obviously, print `hello, world` to the console. Let's check what `putStrLn` is:

```
ghci> :t putStrLn
putStrLn :: String -> IO ()
ghci> :t putStrLn "hello, world"
putStrLn "hello, world" :: IO ()
```

So `putStrLn` is a function that takes a `String` and returns an `IO` monad, which in this context is called an *action*, with produced type of `()` (empty tuple). Actions are functions with side effects (i.e. that change some state), that carry some kind of hidden value inside of them. In this case the state that's being changed is the that of the console (the standard output buffer, in other terms), and that is because we are writing something to it. The value produced by `puStrLn` is `()`, which means "no value", because a function that prints on the console doesn't really produce any value.

The `main` function has always signature `main :: IO a`. In this case `a` is `()` because the return type of `main` is the return type of `putStrLn` (which is the only function called by `main`), and in turn its return type is `IO ()`.

## Impure values
To read a string from the console, we would use `getLine`:

```
ghci> :t getLine
getLine :: IO String
```

So here we have a function that returns an `IO` action producing a `String`. Here the string being produced is clearly the string the user inserted in the console, the one that we are reading; however, we know that we are also changing some state, because this is an action: this is true because after calling this function we are left with something more than what we had before...a new string that didn't existed before!

The value produced by this reading action, however, is confined inside the `IO String` action, and cannot possibly escape it: there's no way to obtain that value as a normal `String` that we can pass around to other functions in our program. This is Haskell strategy to strictly separate *pure* functions (not causing any side effect) from *impure* functions (causing side effects). The result of an impure function calling is always contained inside an action, so it can't escape it and pollute the rest of the program. If a value coming from the outside was used inside a function, that function couldn't possibly be pure anymore, because it wouldn't be true anymore that replacing that function with its value inside the code wouldn't change the behaviour of the program (*referential transparency*): it would, because its value changes each time the program is executed! Calling `readLine` multiple times with the same arguments (no argument) doesn't always return the same value, so we can't write another equivalent expression in place of `readLine`, hoping the program to keep the same behavior! If a function was calling `readLine` from inside it, it couldn't be pure as well, because it's behavior would change at each new call.

## Action binding
Since actions are monads, data contained inside an action can only be transferred to other actions using monads functions. In this way data representing side effects is always clearly defined, and cannot be confused with data from the pure land. For example, we can bind to actions:

```haskell
main = getLine >>= \x -> putStrLn x
```

Here, the first argument of the bind operation is the first action, reading a string from the console, and the second argument is a function (anonymous in our example) that takes the data produced by the first action (that same secret data contained inside the action), does some computation on that, and returns another action, which itself encapsulates another hidden data, maybe depending on the hidden data contained inside the first action. In this example, the data hidden inside the first action is the very string inserted by the user in the console, and we are passing it to `putStrLn` through the `x` identifier. `putStrLn` will print that string back on the console again, and return an empty action `IO ()`.

In this way we have performed some compound computation on an impure value, coming from the outside, without letting it escape the guard of the actions. Using the do notation:

```haskell
main = do
    x <- getLine
    putStrLn x
```

`putStrLn` and `putStr` don't return any interesting value, so when they're chained to some other action, the `>>` function is commonly used:

```haskell
main = putStr "Insert a message: " >> getLine >>= \x -> putStrLn ("You inserted " ++ x)
```

Here we are printing `Insert a message` (without a trailing newline character), with a `putStr` action that returns a `IO ()`, and we want to bind it to the following `getLine` action. However, here we aren't interested in passing the production of `putStr` to `getLine`, because `putStr` doesn't really produce anything anyway. So we use the `>>` operator, to let the second action be performed after the first:

So, in our example, after the first `putStr` prints the first message on the console, the second action `getLine` is immediately performed, without caring about the return of `putStr`. The `getLine` action is binded to the third action, `putStrLn`, by means of the `>>=` operator, that takes as a second argument a function which gets passed the production of `getLine` as an argument. In our case, this (anonymous) function uses this value, called `x` to concatenate it to the end message that gets printed to the console by `putStrLn`.

Of course we could write this with the `do` notation:

```haskell
main = do
    putStr "Insert a message: "
    x <- getLine
    putStrLn ("You inserted " ++ x)
```

Here we aren't giving any name to the return value of `putStr`, because we are not going to use it. However, we could also have done it, if we wanted to:

```haskell
main = do
    foo <- "Insert a message: "
    ...
```

and also

```haskell
main = putStr "Insert a message: " >>= \foo -> getLine ...
```

Here we are simply not using that identifier.

## Nested actions
We can delay the usage of a binding until a later moment, if we nest multiple actions binding inside each other. Let's consider this example:

```
ghci> putStrLn "What's your first name?" >> getLine >>= \firstName -> putStrLn ("Your first name is " ++ firstName) >> putStrLn "What's your last name?" >> getLine >>= \lastName -> putStrLn ("Your last name is " ++ lastName)
```

What if we wanted to print only one final message, like "Your name is firstname lastname"? The `firstName` argument is only available inside the lambda function passed to the first `>>=`, so we can't use it after having fetched the last name. What we can do is moving the second chain of actions, asking the last name, inside that lambda function:

```
ghci> putStrLn "What's your first name?" >> getLine >>= \firstName -> (putStrLn "What's your last name?" >> getLine >>= \lastName -> putStrLn ("Your name is " ++ firstName ++ " " ++ lastName))
```

Here we can still access `firstName` from inside the second lambda function, because this is a closure, and remembers the identifiers defined in the scope it was called in.

This is a good example of when the `do` notation comes in handy. This last program can be rewritten as:

```haskell
main = do
    putStrLn "What's your first name?"
    firstName <- getLine
    do
        putStrLn "What's your last name?"
        lastName <- getLine
        putStrLn $ "Your name is " ++ firstName ++ " " ++ lastName
```

So we see that nested action bindings translate to nested `do` blocks. Notice also that here we are not breaking the rule we said before, that a binding, like `firstName <- getLine` must be immediately used in the next line: in fact here the next line is the whole second `do` block! Turns out that Haskell allows us to rewrite this in an easier way:

```haskell
main = do
    putStrLn "What's your first name?"
    firstName <- getLine
    putStrLn "What's your last name?"
    lastName <- getLine
    putStrLn $ "Your name is " ++ firstName ++ " " ++ lastName
```

Here Haskell can automatically infer that the second `putStrLn` should actually be part of a nested `do` block, so we are allowed not to specify it explicitly.

## *Let* bindings
Since `do` blocks are basically syntactic sugar for lambdas used inside action bindings, we know how to use `let` bindings inside them:

```
ghci> import Data.Char
ghci> putStrLn "What's your first name?" >> getLine >>= \firstName -> (putStrLn "What's your last name?" >> getLine >>= \lastName -> let bigFirstName = map toUpper firstName; bigLastName = map toUpper lastName in putStrLn $ "hey " ++ bigFirstName ++ " " ++ bigLastName ++ ", how are you?")
```

Nothing special here: we are using `let` to bind two expressions to the identifier `bigFirstName` and `bigLastName`, which we are using then in the last `putStrLn`. This is much easier to read like this:

```haskell
import Data.Char
main = do
    putStrLn "What's your first name?"
    firstName <- getLine
    putStrLn "What's your last name?"
    lastName <- getLine
    let bigFirstName = map toUpper firstName
        bigLastName = map toUpper lastName
    putStrLn $ "hey " ++ bigFirstName ++ " " ++ bigLastName ++ ", how are you?"
```

## Using `return`
Consider this other example:

```haskell
main = do
    line <- getLine
    if null line
        then return()
        else do
            putStrLn $ reverseWords line
            main

reverseWords :: String -> String
reverseWords = unwords . map reverse . words
```

Here we are using an `if` block in the same way we would use it inside a lambda function (remember that the whole `if` block is only one expression). The interesting thing here is that we need to return an IO action, so that whole `if` block needs to return an IO action. If the second branch is executed, the last function to be called is `main`, which indeed returns another IO action. But what happens if the first branch is executed? The idea behind the first branch is to exit without doing anything. However, we can't just terminate the expression, because we still need to return an IO action, so we need to create an IO action out of something. This is exactly what the `return` function does.

## Exceptions
The preferred way in Haskell for dealing with computations that can fail, is using things like `Maybe` or `Either`. If a function has a signature like `a -> Maybe b`, it's clear at compile time that there's a chance that this function doesn't return a regular result, and so it can't be confused with another function like `a -> b`, for which it's sure that it will always return a valid result.

However, when dealing with input/output it makes more sense to use conventional exceptions, usually because the kind of exceptional situation arising from the outside world are so hard to foresee that almost always the function where the exceptional situation happen can't know how to handle it. In this case, returning a `Maybe` is of little use, because who called that function likely don't know how to handle that situation, so a better approach is to throw an exception, and the first function knowing how to deal with that will catch it.

Anyway, exceptions can be thrown also by regular functions. If you try to `div` by zero:

```
ghci> 4 `div` 0
*** Exception: divide by zero
ghci> head []
*** Exception: Prelude.head: empty list
```

Now, since Haskell uses lazy-evaluation, when you write an expression like `head []` somewhere in your program, you really don't know when this expression will actually be evaluated, so you can't just put a `catch` or something on the calling function, expecting it to be able to catch that exception, since the order of execution isn't well defined. Input/output functions, on the other hand, have a well defined order of execution, because since they can't be pure, they can't also be lazily evaluated, so the only place where exceptions can be caught is inside IO functions.

Let's say that we want to open a file, but the file has been deleted or something:

```haskell
import System.Environment
import System.IO

main = do
    (fileName:_) <- getArgs
    contents <- readFile fileName
    putStrLn $ "The file has " ++ show (length (lines contents)) ++ " lines!"
```

Here the name of the file we want to open is grabbed from the command line arguments by `getArgs`, and binded to the `fileName` identifier by pattern matching with the tuple `(fileName:_)`. Then we read the contents of the file into `contents` with `readFile`, and finally we show the number of lines contained in the file. If the file doesn't exist, we get an error like `openFile: does not exist (No such file or directory)`.

We could check if the file exists before trying to read it like this:

```haskell
import System.Environment
import System.IO
import System.Directory

main = do
    (fileName:_) <- getArgs
    fileExists <- doesFileExist fileName
    if fileExists
        then do
            contents <- readFile fileName
            putStrLn $ "The file has " ++ show (length (lines contents)) ++ " lines!"
        else do
            putStrLn "The file doesn't exist!"
```

Notice that `doesFileExist` is a `FilePath -> IO Bool`, so we have to extract that boolean value from the returned action using `<-`.

However, we could have managed this situation using exceptions, and in particular using the `catch` function, which is a `IO a -> (IOError -> IO a) -> IO a`. The first argument is the action that could throw an exception, the second is a function that does something with the error and returns another IO action. Then, `catch` returns either the original IO action, if no exception occurred, or the IO action returned by the error handler.

```haskell
import System.Environment
import System.IO
import System.IO.Error

main = toTry `catch` handler

toTry :: IO ()
toTry = do
    (fileName:_) <- getArgs
    contents <- readFile fileName
    putStrLn $ "The file has " ++ show (length (lines contents)) ++ " lines!"

handler :: IOError -> IO ()
handler e
    | isDoesNotExistError e =
        case ioeGetFileName e of
            Just path -> putStrLn $ "Whoops! File does not exist at: " ++ path
            Nothing -> putStrLn "Whoops! File does not exist at unknown location!"
    | otherwise = ioError e
```

Here we catch a "does not exist" error, and rethrow all other kinds of exceptions with `ioError`. Inside the error handler we use `ioeGetFileName` to extract the filename from the exception. This is a `IOError -> Maybe FilePath`, so it can return a file name, or fail to do it, and we handle both cases with a `case` expression.

We can use several `catch`es in sequence:

```haskell
main = do
    toTry `catch` handler1
    thenTryThis `catch` handler2
    launchRockets
```