# 8. Monads

- [*Do* notation](#do-notation)
- [Monads laws](#monads-laws)

As applicative functors are a natural extension of functors, *monads* are a natural extension of applicative functors. Say we have a value with a context, `m a` (which we would represent as a functor or an applicative functor), how do we apply to it a function that takes a regular `a`, and returns a value with a context? This is the function we need:

```haskell
(>>=) :: (Monad m) => m a -> (a -> m b) -> m b
```

Functors and applicative functors work with functions taking values without context, and return values without context. Monads, instead, work with functions taking values without context and returning values with context, and using these functions having to pass values with context to them.

```haskell
class Monad m where
    return :: a -> m a
    (>>=) :: m a -> (a -> m b) -> m b
    (>>) :: m a -> m b -> m b
    x >> y = x >>= \_ -> y
    fail :: String -> m a
    fail msg = error msg
```

For historical reasons, here the type constructor `m` does not derive from `Applicative`, even if it should. The `return` function is like `pure`, encapsulates a regular value inside a monad. The `>>=` function is also called *bind*, and it's like function application, but takes a monadic value and feeds it to a function that takes a normal value and returns a monadic value. Then comes the `>>` function, also known as *then*, which behaves like bind, only discarding the normal value taken by the function. The `fail` function is used by Haskell to manage failure situations.

`Maybe` is an instance of `Monad`:

```haskell
instance Monad Maybe where
    return x = Just x
    Nothing >>= f = Nothing
    Just x >>= f = f x
    fail _ = Nothing
```

The `>>=` function is used to enable the function `f` to be called with a value incapsulated inside a `Maybe`, whereas normally `f` would take a normal value.

```
ghci> Just 9 >>= \x -> return (x * 10)
Just 90
```

So here we have this function `\x -> return (x * 10)` that takes a normal value and return a monad encapsulating the result of multiplying that value for 10, and we want to call this function with the `Maybe` `Just 9`. The result is another `Maybe` containing the result of the multiplication.

## *Do* notation
Since monads are used quite extensively in Haskell, they were given their own special notation, to make coding easier. In particular, this becomes rather necessary when the bind function is called from inside a lambda that was being fed to another bind function:

```
ghci> Just 3 >>= (\x -> Just "!" >>= (\y -> Just (show x ++ y)))
Just "3!"
```

Here the advantage of using `Maybe` is that we can take into account failure, allowing errors to move safely inside the code, until they reach a point where they can be correctly handled. For instance, if one of the three operations we are using, the one producing `3`, the one producing `"!"`, and the final concatenation, failed, we would have a `Nothing` in return, that could be handled by the calling code, instead of a fatal error:

```
ghci> Nothing >>= (\x -> Just "!" >>= (\y -> Just (show x ++ y)))
Nothing
ghci> Just 3 >>= (\x -> Nothing >>= (\y -> >Just (show x ++ y)))
Nothing
ghci> Just 3 >>= (\x -> Just "!" >>= (\y -> Nothing))
Nothing
```

Writing this to a file allows us to use a slightly more readable layout:

```haskell
foo : Maybe String
foo = Just 3   >>= (\x ->
      Just "!" >>= (\y ->
      Just (show x ++ y)))
```

To avoid using all those lambdas, we could try the *do notation*:

```haskell
foo :: Maybe String
foo = do
    x <- Just 3
    y <- Just "!"
    Just (show x ++ y)
```

The `<-` syntax is like we were able to temporarily extract values from monads, and then using them in the last expression. In a do expression, every line is a monadic value: as we've just said, `<-` is used to inspect its value, and bind it to an identifier. This is true except for the last line of a do expression, whose value cannot be bind to an identifier (it wouldn't make sense anyway if we remember what this syntax means in terms of monadic operations).

If we don't need to bind a monadic value to an identifier (like when using the `>>` operator), we just use `_` as an identifier, like `_ <- Nothing`. When binding values to identifier, we can use pattern matching (just like `let` expressions):

```haskell
justH :: Maybe Char
justH = do
    (x:xs) <- Just "hello"
    return x
```

Since, behind the curtains, this is actually a `let` expression, if the pattern matching fail, there's no falling back to the next pattern, as happens in function definition, rather the `fail` function is called (whose default behaviour is rasing an error).

## Monads laws
Monads must obey some laws as well:

- Left identity: `return x >>= f = f x`

here we know that `f` takes a normal value `x` and returns a monad containing some kind of calculation done on `x`. Doing `return x >>= f` means first encapsulating `x` inside a monad with `return x`, and then using `>>=` to pass the result to `f`, that normally would expect a normal value: it's the same thing as `f x`, only with an additional wrapping into the monad. The important idea here is that `return` should put `x` into a minimal context: if this context is really minimal, it shouldn't make a difference as far as how `x` is used is concerned.

- Right identity: `m >>= return = m`

`return` is a function that takes a normal value and returns a monadic one, so it's a perfectly acceptable fit for being used with `>>=`. So here `m` is a monad containing some value, let's call it `x`, and `m` is applied to `return` via the `>>=` function. But this means that `return` is actually being called with `x`, extracted from `m` by `>>=`. Finally, we know what `return x` does: it creates a monad containing `x`, which is indeed the same `m`! The meaning of this law is, again, that `return` puts the value in a minimal context, where its semantic is preserved. Talking about monads, if for some reason `return` introduced a failure, we would have `Nothing` in return, instead of `Just x`: but here we are enforcing that this shouldn't happen, and the value taken by `return` shouldn't be changed in any way.

- Associativity: `(m >>= f) >>= g = m >>= (\x -> f x >>= g)`

So here we have again `m` containing `x`. In the left expression, we are basically applying `f` to `x`, passing through monads first, thanks to `>>=`. The result of this is a monad containing the result of the application of `f` to `x`: this is applied to `g`, again through `>>=`. To the right we have `m` which is applied (trough monads) to a function that first applies `f` to `x` (resulting in a monad containing the result), and then applies the result to `g` via `>>=`. The end result in both cases is that the final monad contains `g (f x)`.