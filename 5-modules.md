# 5. Modules

- [Working with modules](#working-with-modules)
- [Creating new modules](#creating-new-modules)

## Working with modules
A Haskell *module* is a way to encapsulate a set of functions, types, and typeclasses definitions under a common namespace. To have access to the objects exported by a module, we first have to *import* that module. For instance, if we wanted to use the `numUniques` function, that counts how many unique elements are contained in a list, we would first have to import the `Data.List` module, that contains that function's definition:

```haskell
import Data.List
numUniques :: (Eq a) => [a] -> Int
numUniques = length . nub
```

Writing `import Data.List` all objects defined therein are exposed in the current scope. If we wanted to expose only some of a module's functions, we would write:

```haskell
import Data.List (nub, sort)
```

importing only `nub` and `sort` from `Data.List`. To import everything, but some function:

```haskell
import Data.List hiding (nub)
```

Sometimes it happens that some functions defined in a module share their name with other functions in the global namespace, or in another imported module. To import functions from a module, keeping their qualified name, we must do:

```haskell
import qualified Data.Map
```

in this way, to refer to the `filter` function defined in `Data.Map`, we need to write `Data.Map.filter`, preventing it to clash with the `filter` function defined in the `Prelude`. We can also assign aliases to module names, for ease of use:

```haskell
import qualified Data.Map as M
```

allowing us to write just `M.filter`.

## Creating new modules
Let's try to create a module that exports functions for calculating volume and area of geometrical objects. We create a `Geometry.hs` file, specifying first which functions this module is going to export:

```haskell
module Geometry
( sphereVolume
, sphereArea
, cubeVolume
, cubeArea
, cuboidArea
, cuboidVolume
) where

sphereVolume :: Float -> Float
sphereVolume radius = (4.0 / 3.0) * pi * (radius ^ 3)

sphereArea :: Float -> Float
sphereArea radius = 4 * pi * (radius ^ 2)

cubeVolume :: Float -> Float
cubeVolume side = cuboidVolume side side side

cubeArea :: Float -> Float
cubeArea side = cuboidArea side side side

cuboidVolume :: Float -> Float -> Float -> Float
cuboidVolume a b c = rectangleArea a b * 2 + rectangleArea a c * 2 + rectangleArea c b * 2

rectangleArea :: Float -> Float -> Float
rectangleArea a b = a * b
```

Now we'll try to make something a bit more complex. Instead of using a single `Geometry.hs` file for defining our module, create a `Geometry` directory, containing three files, `Sphere.hs`, `Cuboid.hs`, and `Cube.hs`:

`Sphere.hs`

```haskell
module Geometry.Sphere
( volume
, area
) where

volume :: Float -> Float
volume radius = (4.0 / 3.0) * pi * (radius ^ 3)

area :: Float -> Float
area radius = 4 * pi * (radius ^ 2)
```

`Cuboid.hs`

```haskell
module Geometry.Cuboid
( volume
, area
) where

volume :: Float -> Float -> Float -> Float
volume a b c = rectangleArea a b * c

area :: Float -> Float -> Float -> Float
area a b c = rectangleArea a b * 2 + rectangleArea a c * 2 + rectangleArea c b * 2

rectangleArea :: Float -> Float -> Float
rectangleArea a b = a * b
```

`Cube.hs`

```haskell
module Geometry.Cube
( volume
, area
) where

import qualified Geometry.Cuboid as Cuboid

volume :: Float -> Float
volume side = Cuboid.volume side side side

area :: Float -> Float
area side = Cuboid.area side side side
```