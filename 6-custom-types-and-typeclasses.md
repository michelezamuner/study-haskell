# Custom types and typeclasses

- [Record syntax](#record-syntax)
- [Type constructors](#type-constructors)
- [Automatic typeclass implementations](#automatic-typeclass-implementations)
- [Type synonyms](#type-synonyms)
- [Recursive data structures](#recursive-data-structures)
- [Typeclasses](#typeclasses)
- [The `newtype` keyword](#the-newtype-keyword)

The simplest way to define new data types is using the `data` keyword, like:

```haskell
data Bool = False | True
```

The identifier following `data` is the name of the new data type being defined. After the `=` *value constructors* are used to specify which values are comprised in this new type. In this case `Bool` is defined as being equal to `False` or `True`, using the "or" operator `|`.

Value constructors are actually functions returning the actual value. For example, let's say we want to define a new type `Shape` that can represent circles or rectangles:

```haskell
date Shape = Circle Float Float Float | Rectangle Float Float Float Float
```

```
ghci> :t Circle
Circle :: Float -> Float -> Float -> Shape
ghci> :t Rectangle
Rectangle :: Float -> Float -> Float -> Float -> Shape
```

What's happening here is that `Circle` and `Rectangle` are defined as soon as they are included in the `Shape` definition: they don't need to be defined elsewhere. This `Shape` type can be now used inside functions, like:

```haskell
surface :: Shape -> Float
surface (Circle _ _ r) = pi * r ^ 2
surface (Rectangle x1 y1 x2 y2) = (abs $ x2 - x1) * (abs $ y2 - y1)
```

Here is not possible to write a function that takes a `Circle` and returns a `Float`, because `Circle` is not a type, nor `Rectangle`: only `Shape` is.

If we want our type to be able to be printed as a string, we should make it derive from `Show`:

```haskell
data Shape = Circle Float Float Float | Rectangle Float Float Float Float deriving (Show)
```

To clarify things a bit more, we can define a `Point` type:

```haskell
data Point = Point Float Float deriving (Show)
data Shape = Circle Point Float | Rectangle Point Point deriving (Show)
```

Inside `Point` definition, since we have only one value constructor, it's usual to name it the same as the type, in this case `Point` as well.

```haskell
surface :: Shape -> Float
surface (Circle _ r) = pi * r ^ 2
surface (Rectangle (Point x1 y1) (Point x2 y2)) = (abs $ x2 - x1) * (abs $ y2 - y1)
```

When exporting data types, we need to specify which value constructors we want to export as well, or write `..` to export them all:

```haskell
module Shapes
( Point(..)
, Shape(..)
, surface
) where
```

Writing `Shape(Rectangle)` would have exported only the `Rectangle` value constructor. Writing `Shape` alone would have exported no value constructor: in this case we would need other exported function building values for us.

## Record syntax
The basic syntax for defining new data types can get quite cumbersome when a type contains several properties. For instance, if we defined a `Person` type like this:

```haskell
data Person = Person String String Int Float String String deriving (Show)
```

the first problem is that it's not immediate that the parameters for the value constructor actually refer to first name, last name, age, height, phone number and favorite ice-cream flavor. Furthermore, if we wanted to define getter functions, we had to do like this:

```haskell
firstName :: Person -> String
firstName (Person firstname _ _ _ _ _) = firstname

lastName :: Person -> String
lastName (Person _ lastname _ _ _ _) = lastname
```

and so on. A better way to define types in cases like this, is using the *record syntax*:

```haskell
data Person = Person { firstName :: String
                     , lastName :: String
                     , age :: Int
                     , height :: Float
                     , phoneNumber :: String
                     , flavor :: String
                     } deriving (Show)
```

Other than being far more clear, this automatically provides getter functions:

```
ghci> :t flavor
flavor :: Person -> String
ghci> :t firstName
firstName :: Person -> String
```

and allows us to construct new values passing parameters in the order we like:

```haskell
data Car = Car { company :: String, model :: String, year :: Int } deriving (Show)
```

```
ghci> Car {company="Ford", model="Mustang", year=1967}
Car {company = "Ford", model = "Mustang", year = 1967}
```

## Type constructors
Value constructors seen previously are no different than constructors normally found inside classes of object oriented languages: the class define a type, and calling its constructor instantiates that class, creating a new value of that type. In Haskell, though, we can also define *type constructors* which, rather than construct new values of a type, construct new types, not so differently than what templates do in other languages.
```haskell
data Maybe a = Nothing | Just a
```

Here we are still using the keyword `data`, like in value constructors, but this time we are also providing a *type parameter* `a`: this tells Haskell that `Maybe` should be a type constructor. The role of the type parameter is to represent a type, not unlike the `<T>` used in templates definitions in other languages. When we actually use `Maybe` in our code, that `a` will be replaced by an actual type, like `Maybe Int`, `Maybe Car`, `Maybe String`, etc.

So the type constructor build a new type from an existing one. For instance, `Maybe Int` can be `Nothing` or `Just Int`, so if we ask the type of `Just 84`, we get `(Num t) => Maybe t` as a response (because Haskell can't decide which type is `84`, so it uses the `Num` typeclass).

The type of `Nothing` is `Maybe a`, so it's type is polymorphic, like the type of `5`, which can be `Int` or `Double`. The type of `5` is really `Num a`, because it can be any `Num` type, the same way `Nothing` is of type `Maybe a` because `Nothing` can really be used in place of any `Maybe`.

Using a polymorphic type (that is a type defined with a type parameter) makes sense only when all the operations defined inside that type make sense for any type of value used. For example, our `Car` type wouldn't make much sense if it was changed to a polymorphic type, because all the functions we are using with it really make sense only with one set of data types:

```haskell
tellCar :: Car -> String
tellCar (Car {company = c, model = m, year = y}) = "This " ++ c ++ " " ++ m ++ " was made in " ++ show y
```

If `Car` was polymorphic, like `data Car a b c`, we would have to change the function signature to this:

```haskell
tellCar :: (Show a) => Car String String a -> String
```

So we are only accepting `Car`s that have `String`s for company and model (but in theory we could define type of cars having numers, or any other types, for company and model!), and any `Show`able type for year. Given the meaning of this function, this is clearly too much, and unnecessarily, abstract, and this is because there's no point in making `Car` polymorphic in the first place.

Type parameters make sense when the type of values we will work with isn't really important for doing the work: the concept of "list" doesn't depend on the actual type of its element...it's clear what a list of "things" is, and how it should behave, regardless of the actual type of those "things"; a `Maybe` is something that can have a value, or not, regardless of the type of this value, and so on.

### Typeclass constraints in type constructors
We could add typeclass constraints to type constructors declarations, for example:

```haskell
data (Ord k) => Map k v = ...
```

this means that `Map` types can only be constructed using types that are `Ord` as keys. Now, this looks like a good idea at first, thinking that all `Map` function should involve `Ord` key types. However, it turns out that these `Map` types could be used also in functions that don't really care if the keys can be ordered or not: for example, `toList` takes a map and converts it to an associative list. If map keys where defined as `Ord`, we should repeat this constraint in every function using maps, even where it's not necessary:

```haskell
toList :: (Ord k) => Map k a -> [(k, a)]
```

here, despite `toList` doesn't care if keys can be ordered, because it doesn't need to compare keys, we still are forced to specify that keys are `Ord`, because we defined `Map` like this.

So, a very strong convention in Haskell is to never add typeclass constraints in data declaration, because they are rarely useful, and in the end they only make using that type constructor more annoying.

## Automatic typeclass implementations
Defining a type that implements a typeclass is not much different than defining a class that implements an interface in object-oriented languages. Typically we need to add definitions for all the (virtual) typeclass functions to our type.

However, Haskell can make our type automatically implement the following typeclasses, without the need for us to override functions explicitly: `Eq`, `Ord`, `Enum`, `Bounded`, `Show`, `Read`. Consider this:

```haskell
data Person = Person { firstName :: String
                     , lastName :: String
                     , age :: Int
                     } deriving (Eq)
```

The `Eq` typeclass define the functions `==` and `/=`, and so we should be required to override them for our `Person` type. However, for the `Eq` typeclass (and the others listed above), Haskell already knows how to use `==` and `/=` for any type: for example, in this case if we were to write `a == b` with `a` and `b` instances of `Person`, Haskell would just check if each property of the two `Person`s match, calling the `==` implementation for those properties' types. For example, it would check if `firstName` of `a` is equal to `firstName` of `b` (using the `String` implementation of `==`), and so on.

Consider this example:

```haskell
data Day = Monday | Tuesday | Wednesday | Thursday | Friday | Saturday | Sunday
```

since all value constructors are *nullary* (take no parameters), we can make `Day` implement `Enum`: this is because `Enum` means that values have predecessors and successors, and since here values are always the same (because they can't be constructed differently depending on which parameters they are constructed with, since they take no parameter), the predecessor/successor relationship is inferred by the order value constructors are found in the type definition. This means that `Monday` is predecessor of `Tuesday`, `Thursday` is successor of `Wednesday`, etc. So:

```haskell
data Day = Monday | Tuesday | Wednesday | Thursday | Friday | Saturday | Sunday
           deriving (Eq, Ord, Show, Read, Bounded, Enum)
```

Since it implements `Show` and `Read`, we can cast these values from and to strings:

```
ghci> Wednesday
Wednesday
ghci> show Wednesday
"Wednesday"
ghci> read "Saturday" :: Day
Saturday
```

Implementing `Eq` and `Ord`, we can use comparison operators:

```
ghci> Saturday == Sunday
False
ghci> Saturday == Saturday
True
ghci> Saturday > Friday
True
ghci> Monday `compare` Wednesday
LT
```

Being `Bounded`, we have highest and lowest values:

```
ghci> minBound :: Day
Monday
ghci> maxBound :: Day
Sunday
```

Finally, being `Enum` we can get predecessors and successors:

```
ghci> succ Monday
Tuesday
ghci> pred Saturday
Friday
ghci> [Thursday .. Sunday]
[Thursday, Friday, Saturday, Sunday]
```

Again, all these functions can be used with our new type because Haskell already know how to implement `Eq`, `Ord`, `Enum`, `Bounded`, `Show` and `Read`.

## Type synonyms
We know that `String` is a *type synonym* of `[Char]`: this means that they can be used interchangeably:

```haskell
type String = [Char]
```

This is useful to add expressiveness to basic types:

```haskell
type PhoneNumber = String
type Name = String
type PhoneBook = [(Name, PhoneNumber)]
```

One thing is talking about a list of pairs of strings, another one is talking about a list of pairs of names and phone numbers, even if they are the same thing.

Type synonyms can be parameterized, for example if we want to define an associative list as a list of pairs, we could do:

```haskell
type AssocList k w = [(k,v)]
```

so we can define a function that takes a value from an associative list, having the signature:

```haskell
(Eq k) => k -> AssocList k v -> Maybe v
```

so this function would take an `Eq` key and an associative list, and returns the value corresponding to that key (or `Nothing` if not found).

Here, type synonyms could be only really used with concrete types, without parameters. What we did just above was to use another type constructor, specifically `AssocList k w`. This type constructor takes two type parameters `k` and `v` and returns a concrete type, like `AssocList Int String`.

Parametric type synonyms can be partially applied, to have back another parametric type synonyms, with some parameter less:

```haskell
type IntMap v = Map Int v
```

which is equivalent to:

```haskell
type IntMap = Map Int
```

## Recursive data structures
We've just seen type constructors, which are able to create new types from existing ones, named type parameters. Something that can be thought of in that regard is of having a type parameter being of the same type of the type constructor. This is actually what happens with lists:

```haskell
data List a = Empty | Cons a (List a) deriving (Show, Read, Eq, Ord)
```

here `Cons` is the `:` operator that append an element in front of a list. This definition says that a list is either an empty list, or an element of type `a` appended to the front of another list `List a`. Since inside the definition of `List` we use the same type `List` that we are defining, this is called a *recursive data structure*. An equivalent way of defining lists could be, using the record syntax:

```haskell
data List a = Empty | Cons { listHead :: a, listTail :: List a} deriving (Show, Read, Eq, Ord)
```

### Defining operators
We could also define `List` using a new cons operator:

```haskell
infixr 5 :-:
data List a = Empty | a :-: (List a) deriving (Show, Read, Eq, Ord)
```

here `infixr` defines a new infix operator with right associativity, and precedence `5` (for instance `*` as precedence 7, and + has precedence 6: greater precedence means tighter binding), and we use it in place of `Cons` or `:`. Once we defined this new list type, we can write:

```
ghci> 3 :-: 4 :-: 5 :-: Empty
(:-:) 3 ((:-:) 4 ((:-:) 5 Empty))
```

And a new concatenation operator:

```haskell
infixr 5 .++
(.++) :: List a -> List a -> List a
Empty .++ ys = ys
(x :-: xs) .++ ys = x :-: (xs .++ ys)
```

so that:

```
ghci> let a = 3 :-: 4 :-: 5 :-: Empty
ghci> let b = 6 :-: 7 :-: Empty
ghci> a .++ b
(:-:) 3 ((:-:) 4 ((:-:) 5 ((:-:) 6 ((:-:) 7 Empty))))
```

The interesting thing here is the pattern matching `(x :-: xs)`. What we are doing with this pattern matching, is asking: does the parameter have this form? What this means in terms of Haskell, is asking if that parameter has that constructor. Pattern matching actually works matching constructors, so when we pattern match to `(a,b)` we are asking if the parameter was built using the tuple constructor `(a,b)`; when we pattern match to `[]` we are asking if the value was constructed with the `[]` constructor, and so on.

## Typeclasses
As we said before, typeclasses are like interfaces in object-oriented languages: they define a behaviour that compatible types should have, and actual types belonging to those typeclasses implement the functions thus defined. For example, the `Eq` typeclass defines the behaviour of types that can be tested for equality, specifying that those type should define the `==` and `/=` functions:

```haskell
class Eq a where
    (==) :: a -> a -> Bool
    (/=) :: a -> a -> Bool
    x == y = not (x /= y)
    x /= y = not (x == y)
```

To define a new typeclass we use the keyword `class`. `Eq` is the name of the new typeclass, and `a` is the type parameter, representing the actual type that will be implementing this interface. After this header, several functions are defined. The function bodies are not required to be implemented (although they can be!), but their type declaration must be included. In this definition of `Eq` we added implementations for `==` and `/=`: however, these are not actual implementations that can be used to execute `==` with, for example, the `List` type, because here we can't possibly know what a `List` is, let alone what does equating it to something else means. The implementations we added act more like constraints.

Consider this type:

```haskell
data TrafficLight = Red | Yellow | Green
```

In the previous examples, we would have added some typeclass implementation here, like `deriving (Eq, Show)`. However, in this example we aren't doing that, because we don't want to rely on the automatic implementation, but we want to explicitly write the implementation instead:

```haskell
instance Eq TrafficLight where
    Red == Red = True
    Green == Green = True
    Yellow == Yellow = True
    _ == _ = False
```

Since in the typeclass declaration `==` was defined in terms of `/=`, in the typeclass implementation we can just define the behaviour of one of the two, `==` in this case. We have to write implementations for each typeclass our type belongs to. For instance, if `TrafficLight` was implementing `Show` also, we would have to write its definition too:

```haskell
instance Show TrafficLight where
    show Red = "Red light"
    show Yellow = "Yellow light"
    show Green = "Green light"
```

Typeclasses can derive from other typeclasses:

```haskell
class (Eq a) => Num a where
    ...
```

here we are defining `Num` as deriving from `Eq`, meaning that each type that is a `Num` is also a `Eq`. To be more precise, this syntax is actually stating that the type `a` should already be an instance of `Eq`.

To define a type constructor implementing a typeclass:

```haskell
instance (Eq m) => Eq (Maybe m) where
    Just x == Just y = x == y
    Nothing == Nothing = True
    _ == _ = False
```

Here it's important to include `(Eq m)` because we are using `==` in the definition, so we must be sure that we can call `==` on `m`.

To see what the instances of a typeclass are we can use the `:info` command from `ghci`.

## The `newtype` keyword
`ZipList`s are a way to apply lists of functions to lists of values, so that the first function is applied to the first value, the second function to the second value, and so on; or that given a function taking two parameters, and two lists of values, the output is another list where the first element is the result of applying the function to the two first values, the second element is the result of applying it to the two second values, and so on. For instance, given the `(+)` function and the lists `[1,2,3]` and `[5,6,7]`, we get as result `[6,8,10]`.

The `ZipList` type, defined in `Control.Applicative`, has a constructor `ZipList`, with only one field, which is a list of values. There are ways to apply functions to zip lists to get the behavior described above, and these are based on the concept of applicative functors. To get the list contained inside a zip list, we use the `getZipList` function:

```haskell
:t getZipList
getZipList :: ZipList a -> [a]
```

so that we could write the definition of `ZipList` as:

```haskell
data ZipList a = ZipList { getZipList :: [a] }
```

Turns out that it happens quite often that we want to make a type as a wrapper for another type, adding some functionalities to it, just like zip lists are a wrapper for regular lists. Haskell provides the `newtype` keyword for these occasions:

```haskell
newtype ZipList a = ZipList { getZipList :: [a] }
```

This tells Haskell that we just want to wrap a type into another one, and this allows it to make some optimization. The limitation of `newtype`, however, is that you can just use one value constructor, so you can't define things like `data Profession = Fighter | Archer | Accountant`.

We can pattern match over `newtype`s to get the underlying type:

```
ghci> newtype Pair b a = Pair { getPair :: (a,b) }
ghci> let { f (Pair (x,y)) = Pair (2*x, y) }
ghci> getPair (f (Pair (1,2)))
(2,2)
```

here we are defining the `f` function so that it takes a `Pair` and returns another `Pair` with the first element doubled, and to extract the tuple inside the `Pair` we use the pattern matching `Pair (x,y)`.