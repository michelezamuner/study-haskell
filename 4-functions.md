# 4. Functions

- [Pattern matching](#pattern-matching)
- [Guards, wheres and lets](#guards-wheres-and-lets)
- [Case expressions](#case-expressions)
- [Recursion](#recursion)
- [Curried functions](#curried-functions)
- [Higher-order functions](#higher-order-functions)
- [Lambda functions](#lambda-functions)
- [Folds](#folds)
- [The *function application* function](#the-function-application-function)
- [Function composition](#function-composition)

## Pattern matching
Function in Haskell share a lot with their mathematical counterparts, and not only for their pureness and referential transparency, but also for some syntax. One important aspect of Haskell functions is *pattern matching*. Often mathematical functions split their definition among several cases, according to the values of variables. For instance, we can define a function `f` such as `f` is `x + 2` for `x < 2` and `x^2` for `x >= 2`.

To do this in Haskell, we just provide multiple definitions for the same function, specifying different values for the arguments:

```haskell
sayMe :: (Integral a) => a -> String
sayMe 1 = "One!"
sayMe 2 = "Two!"
sayMe 3 = "Three!"
sayMe 4 = "Four!"
sayMe 5 = "Five!"
sayMe x = "Not between 1 and 5"
```

When calling a function defined in such way, Haskell scrolls the list of patterns we provided in place of arguments (in this case, `1`, `2`, ..., `x`), until the actual value used in the function invocation matches one of these patterns. In this case, if we called `sayMe 4`, Haskell would have checked the first pattern, `1`, seen that `4` doesn't match `1`, moved on to `2`, and so on, until reaching `4`, where the value `4` indeed matches the pattern. After finding the matching pattern, the corresponding function definition is used. Here it's important to note that if we moved the last pattern, `x`, to the top, it would be always executed, because Haskell stops its search as soon as it finds a matching pattern, without looking at the next ones. If we call a function with an actual argument that doesn't match any of the defined patterns, an exception is raised (which means, a runtime error).

Pattern matching can get more complicated. Let's say we want to create a function that takes to pairs, representing coordinates of points in a plane, and calculate their dot product, one simple way to do this would be:

```haskell
addVectors :: (Num a) => (a, a) -> (a, a) -> (a, a)
addVectors a b = (fst a + fst b, snd a + snd b)
```

Here we are referring to the two pairs given as parameters as `a` and `b`, and using `fst` and `snd` to extract respectively their first and second elements. However, pattern matching let us get much more expressive than this:

```haskell
addVectors :: (Num a) => (a, a) -> (a, a) -> (a, a)
addVectors (x1, y1) (x2, y2) = (x1 + x2, y1 + y2)
```

While with the `a b` notation we catch all kinds of inputs, with `(x1, y1) (x2, y2)` we catch all inputs that have the shape of pairs. However, this is harmless since we are stating in the signature that we want pairs as arguments. Writing variables like this allows us to avoid calling `fst` and `snd` to get the firsts and seconds elements, because this operation is automatically performed during the pattern matching.

In addition to tuples, we can also use lists and lists operators in pattern matching, like `[]` and `x:xs`:

```haskell
head' :: [a] -> a
head' [] = error "Can't call head on an empty list!"
head' (x:_) = x
```

Here, if we provide a non-empty list to this function, while trying to match the second pattern, Haskell would automatically split the list into its head, assigning it to `x`, and its tail, assigning it to `_`, which means discarding it. Then we return `x`, which is exactly the head of the list passed in. Here we are surrounding the pattern `x:_` between parenthesis because we are binding more than one variable (`_` is considered a variable as well).

```haskell
tell :: (Show a) => [a] -> String
tell [] = "The list is empty"
tell (x:[]) = "The list has one element: " ++ show x
tell (x:y:[]) = "The list has two elements: " ++ show x ++ " and " ++ show y
tell (x:y:_) = "This list is long. The first two elements are: " ++ show x ++ " and " ++ show y
```

Here we could have replaced `x:[]` and `x:y:[]` with `[x]` and `[x,y]`, respectively.

Variables inside patterns, like `x` and `y` inside `x:y:[]`, are there to be reused inside the body of the function, corresponding to that pattern. However, it can happen that inside the body we also need a reference to the entire pattern. For example, imagine we were writing a function like this:

```haskell
capital :: String -> String
capital "" = "Empty string"
capital (x:xs) = "The first letter of " ++ (x:xs) ++ " is " ++ [x]
```

Here we are repeating the `x:xs` pattern multiple times. However, Haskell provides an easier way to do that, creating a reference to the pattern, using so-called *as patterns*:

```haskell
capital :: String -> String
capital "" = "Empty string"
capital all@(x:xs) = "The first letter of " ++ all ++ " is " ++ [x]
```

where the notation `all@(x:xs)` means that `all` is a reference that can be used in place of `(x:xs)`.

## Guards, wheres and lets
*Guards* are a way to split the body of a function throught several conditionals, much like an `if` structure. Let's see an example:

```haskell
bmiTell :: (RealFloat a) => a -> String
bmiTell bmi
    | bmi <= 18.5 = "You're underweight"
    | bmi <= 25.0 = "You're normal"
    | bmi <= 30.0 = "You're overweight"
    | otherwise   = "You're obese"
```

A guard is a boolean expression using function arguments, introduced by a `|` character following the name of the function. If the guard evaluates to `False`, the next guard is checked; otherwise, the body following the guard is executed. The `otherwise` guard is always `True`, and for this reason catches all cases not caught by previous guards.

```haskell
bmiTell :: (RealFloat a) => a -> a -> String
bmiTell weight height
    | weight / height ^ 2 <= 18.5 = "You're underweight"
    | weight / height ^ 2 <= 25.0 = "You're normal"
    | weight / height ^ 2 <= 30.0 = "You're overweight"
    | otherwise                   = "You're obese"
```

Here we replaced `bmi` with a formula calculating it. However, we had to repeat that formula for each guard. To avoid this repetition, we can use a `where` clause:

```haskell
bmiTell :: (RealFloat a) => a -> a -> String
bmiTell weight height
    | bmi <= 18.5 = "You're underweight"
    | bmi <= 25.0 = "You're normal"
    | bmi <= 30.0 = "You're overweight"
    | otherwise   = "You're obese"
    where bmi = weight / height ^ 2
```

After the `where` clause several names or functions can be defined, and they are visible across the guards:

```haskell
bmiTell :: (RealFloat a) => a -> a -> String
bmiTell weight height
    | bmi <= skinny = "You're underweight"
    | bmi <= normal = "You're normal"
    | bmi <= fat    = "You're overweight"
    | otherwise     = "You're obese"
    where bmi = weight / height ^ 2
          skinny = 18.5
          normal = 25.0
          fat = 30.0
```

Names defined in the `where` section are only visible inside this function definition. They also need to be aligned at a single column, otherwise Haskell can't tell what block they are part of. `where` blocks aren't shared across function bodies of different patterns. `where` bindings can also make use of pattern matching, like this:

```haskell
-- ...
where bmi = weight / height ^ 2
    (skinny, normal, fat) = (18.5, 25.0, 30.0)
```

`where` bindings need not to come together with guards, actually. They can also be used on their own to define names used inside the function body:

```haskell
initials :: String -> String -> String
initials firstname lastname = [f] ++ ". " ++ [l] ++ "."
    where (f:_) = firstname
          (l:_) = lastname
```

Functions can be defined inside `where` blocks as well:

```haskell
calcBmis :: (RealFloat a) => [(a,a)] -> [a]
calcBmis xs = [bmi w h | (w,h) <- xs]
    where bmi weight height = weight / height ^ 2
```

`let` bindings let define variables, but their scope is more limited than `where` bindings: for example, they aren't visible across guards.

```haskell
cylinder :: (RealFloat a) => a -> a -> a
cylinder r h =
    let sideArea = 2 * pi * r * h
        topArea = pi * r ^ 2
    in sideArea + 2 * topArea
```

Here the body of the function is defined in the `in` part, and the variables it's using are defined in the `let` part. Actually this could have been done using also `where` bindings; however, while `where` bindings are syntactic constructs used to replace names with expressions, `let` bindings are expressions themselves, so they can be but almost anywhere:

```
ghci> 4 * (let a = 9 in a + 1) + 2
42
```

This is useful to avoid repetition, like in:

```
ghci> [let square x = x * x in (square 5, square 3, square 2)]
[(25,9,4)]
```

To bind to several variables inline, we separate them with semicolons, whereas on multiple lines we would have put them at the same column:

```
ghci> (let a = 100; b = 200; c = 300 in a * b * c, let foo = "Hey "; bar = "there!" in foo ++ bar)
(6000000,"Hey there!")
```

Pattern matching works with `let` bindings as well:

```
ghci> (let (a,b,c) = (1,2,3) in a+b+c) * 100
600
```

and in list comprehension:

```haskell
calcBmis :: (RealFloat a) => [(a, a)] -> [a]
calcBmis xs = [bmi | (w,h) <- xs, let bmi = w / h ^ 2, bmi >= 25.0]
```

Omitting the `in` part, here means that the output function, and all predicates following the `let` can see the bindings. Had we used a `in` part here, only that part would have been able to see the bindings. In general, if we omit the `in`, the bindings are visible from the whole scope containing the `let`.

## Case expressions
Turns out that pattern matching in function definition is a special case of *case expressions*. In particular, this definition:

```haskell
head' :: [a] -> a
head' [] = error "No head for empty lists!"
head' (x:_) = x
```

Can be rewritten as:

```haskell
head' :: [a] -> a
head' xs = case xs of [] -> error "No head for empty lists!"
                      (x:_) -> x
```

Case expression, however, can also be used outside function definitions:

```haskell
describeList :: [a] -> String
describeList xs = "The list is " ++ case xs of [] -> "empty."
                                               [x] -> "a singleton list."
                                               xs -> "a longer list."
```

## Recursion
Unlike imperative languages, Haskell doesn't have `for` or `while` loops. What's instead used for repeating the same operations multiple times is *recursion*. This is perfectly in line with functional programming philosophy of not defining "how" to do things, but "what" things are.

For example, the `maximum` function takes a list and returns its maximum element (provided that it's a list of elements that can be ordered). To define this function in an imperative manner, one would make a loop scrolling through the list, each time comparing the current element with the current maximum, replacing the latter if the former is greater. This is "how" one finds the maximum value.

However, functional programming is concerned about "what" the maximum value is, and from this point of view, the maximum value of a list is the list's element, if the list only has that one element, otherwise it is the maximum between the head of the list, and the maximum of the tail:

```haskell
maximum' :: (Ord a) => [a] -> a
maximum' [] = error "maximum of empty list"
maximum' [x] = x
maximum' (x:xs)
    | x > maxTail = x
    | otherwise = maxTail
    where maxTail = maximum' xs
```

So here, a part from the usual error check, we are saying that the maximum is `x` if that's the only element, otherwise we first use pattern matching to separate the head from the tail `x:xs`, and we define two guards, the first returning the head if that is greater than the maximum of the tail, the second returning the maximum of the tail if that's the greater. Finally, we define what the maximum of the tail is, as a `where` binding, and the maximum of the tail is obviously the same `maximum'` function called on the tail.

We can also use the function `max`, that returns the maximum of two numbers, to get an even clearer definition:

```haskell
maximum' :: (Ord a) => [a] -> a
maximum' [] = error "maximum of empty list"
maximum' [x] = x
maximum' (x:xs) = max x (maximum' xs)
```

## Curried functions
In Haskell, all functions take at most one parameter, officially. When we define and use functions that take more than one parameter, we are actually using syntactic sugar for *curried functions*. Curried functions are functions that are returned from another function, to complete the computation. For example, the `max` function actually takes only one parameter, the first operand, and returns a curried function taking the second parameter. In fact, we could write:

```
ghci>(max 4) 5
```

since we are applying `5` to the function returned by `max 4`. Furthermore, the type of `max 4` is:

```
ghci>:t max 4
max 4 :: (Num a, Ord a) => a -> a
```

it's another function taking a number (the second operand of `max`) and returning another number (the maximum of the two). In general, a space in Haskell is like an operator doing function application, so writing `max 4` means applying `4` to the function `max`, and writing `max 4 5` means applying `5` to the function `max 4`. This also explains why types in Haskell has that awkward syntax that doesn't distinguish between arguments types and return types, like `max :: (Ord a) => a -> a -> a`. This is because this type could as well be written as `max :: (Ord a) => a -> (a -> a)`, meaning that `max` takes an `Ord` and returns (this is what `->` actually means) another function taking another `Ord` and returning still another `Ord`.

If we call a function with too few parameters, like `max 4` we get back a *partially applied* function, taking as many parameters as we left out. This is useful when we want to quickly create a new function, starting from an existing one, for example:

```haskell
compareWithHundred :: (Num a, Ord a) => a -> Ordering
compareWithHundred x = compare 100 x
```

We actually can omit the `x` here:

```haskell
compareWithHundred :: (Num a, Ord a) => a -> Ordering
compareWithHundred = compare 100
```

This can work only when the variable is the last argument. To partially apply infix operator or functions, we need to use sections, using the prefix form of that operator or function:

```haskell
divideByTen :: (Floating a) => a -> a
divideByTen = (/10)
```

## Higher-order functions
Functions being first-class citizens in Haskell, not only they can be returned from other functions, like we just saw, be they can also be passed as arguments to other functions:

```haskell
applyTwice :: (a -> a) -> a -> a
applyTwice f x = f (f x)
```

Here we need to wrap the types of the passed function between parenthesis, because otherwise that would mean that our function is taking three arguments.

A couple of very useful *higher-order functions* (functions taking other functions as arguments) are `map` and `filter`:

- `map`: takes a function and a list, and returns a list whose elements are all the first list's elements, after having applied the function to them:

```haskell
map :: (a -> b) -> [a] -> [b]
map _ [] = []
map f (x:xs) = f x : map f xs
```

```
ghci map (+3) [1,5,3,1,6]
[4,8,6,4,9]
```

- `filter`: takes a predicate function (telling if something is true or not, thus returning a boolean) and a list, and returns the list of elements that satisfy the predicate:

```haskell
filter :: (a -> Bool) -> [a] -> [a]
filter _ [] = []
filter p (x:xs)
    | p x = x : filter p xs
    | otherwise = filter p xs
```

```
ghci> filter (>3) [1,5,3,2,6]
[5,6]
```

However, both these functionalities could be achieved using list comprehension, but there's no rule telling which one is better, it's often just a matter of readability.

## Lambda functions
Lambda functions are anonymous functions, and they aren't given a name usually because they need to be used only in one place, like passing it to a higher-order function.

Let's talk about Collatz sequences. Given a natural number, if that number is even, we divide it by two; if it's odd, we multiply if by 3 and then add 1 to that. We take the resulting number and apply the same computation to it, and so on for every result. For example, starting with 13, we get 13, 40, 20, 10, 5, 16, 8, 4, 2, 1.

We want to write a function that tells us, for all starting numbers between 1 and 100, how many sequences have a length greater than 15. First, we need to produce a Collatz chain:

```haskell
chain :: (Integral a) => a -> [a]
chain 1 = [1]
chain n
    | even n = n:chain (n `div` 2)
    | odd n = n:chain (n*3 + 1)
```

Then, to know which sequences are longer that 15 elements:

```haskell
numLongChains :: Int
numLongChains = length (filter isLong (map chain [1..100]))
    where isLong xs = length xs > 15
```

So we first get all Collatz sequences starting from 1 to 100, mapping `chain` to `[1..100]`; then we filter the result with the `isLong` function, defined below as telling if the list has more than 15 elements; last we get the length of the filtered lists, which is also how many sequences have more than 15 elements.

In the `numLongChains` function definition, we see that `isLong` is used only once: as a predicate for the `filter` call. This is a typical case where we would want to use a lambda:

```haskell
numLongChains :: Int
numLongChains = length (filter (\xs -> length xs > 15) (map chain [1..100]))
```

Lambda functions are introduced by the `\` character, followed by the list of arguments, followed by the `->` meaning "return" as usual, finally followed by the body of the function.

Lambdas are often used where partial application falls short, but it's important not to overuse them, since partial application, in the cases where it's applicable, is generally more readable. For instance, `map (+3) [1,6,3,2]` is certainly more readable than `map (\x -> x + 3) [1,6,3,2]`.

Lambdas can take any number of parameters:

```
ghci> zipWith (\a b -> (a * 30 + 3) / b) [5,4,3,2,1] [1,2,3,4,5]
[153.0,61.5,31.0,15.75,6.6]
```

and can also use pattern matching, although only one pattern can be specified, and if there's no match, a runtime error occurs:

```
ghci> map (\(a,b) -> a + b) [(1,2),(3,5),(6,3)(2,6)(2,5)]
[3,8,9,8,7]
```

Lambdas need to be surrounded by parenthesis, unless their definition extends all the way to the right:

```haskell
addThree :: (Num a) => a -> a -> a -> a
addThree = \x -> \y -> \z -> x + y + z
```

which is equivalent to `addThree x y z = x + y + z`.

## Folds
Let's look again at a typical recursive function:

```haskell
sum' :: (Num a) => [a] -> a
sum' [] = 0
sum' (x:xs) = x + sum' xs
```

Here we see a very common pattern used among recursive functions: we have a list `[a]`, we separate the head from the tail `(x:xs)`, then we apply a function to the head `(+)` and we recursively apply the function again to the tail, `sum' xs`. This pattern is so common that it has been encapsulated inside its own concept: the *fold*.

A fold is a function that takes a binary function, a starting value (*accumulator*), and a list to fold up. The binary function is first called with the accumulator and the first element of the list, and returns a value that is used as the new accumulator. Then the binary function is called again with the new accumulator, and the second element of the list, and so on. We see that the fold function "folds" the list starting from the first element, using each element as the second argument of the binary function, while keep updating the accumulator in the meanwhile. If the list is empty, the result is the starting value of the accumulator.

Haskell provides two folds, `foldl` and `foldr`:
- `foldl` folds the list starting from the left:

```haskell
sum' :: (Num a) => [a] -> a
sum' xs = foldl (\acc x -> acc + x) 0 xs
```

Here the accumulator starts from `0`, and at the first iteration is added to the first element of the list. The result (which is still the first element of the list, since it's been added to 0), is used as the new accumulator. This means that at the second iteration the first element of the list (stored in the accumulator) is added to the second element of the list (because it's the second iteration), and so on.

However, here we can partially apply `foldl`, since the argument coming from the user, `xs`, is the last argument. Furthermore, here we are using a lambda where another partial application would have fit:

```haskell
sum' :: (Num a) => [a] -> a
sum' = foldl (+) 0
```

- `foldr` works the same as `foldl`, but the list is folded from the left, so at the first iteration the binary function is applied to the accumulator and the last value of the list (instead of the first); at the second iteration the current accumulator is used together with the second last element of the list, and so on. Also, the binary function takes the accumulator as the second parameter, not the first, as in `\x acc -> ...`.

- `foldl1` and `foldr1` work the same as `foldl` and `foldr` respectively, but they don't take a starting value for the accumulator, using the first or last element of the list, respectively, as starting values instead. The sum function can be thus defined as `sum = foldl1 (+)`. However, if they are called with an empty list, a runtime error is thrown, because they depend on the first value as starting value.

- `scanl` and `scanr` work just like `foldl` and `foldr`, but they return all the intermediate accumulator states:

```
ghci> scanl (+) 0 [3,5,2,1]
[0,3,8,10,11]
```

## The *function application* function
The `$` function is the *function application* function:

```hakell
($) :: (a -> b) -> a -> b
f $ x = f x
```

This works exactly as the space operator, except for it having the lowest precedence, instead of the highest, and being right-associative, instead of left-associative. This means that

```haskell
sum (map sqrt [1..130])
```

can be rewritten as

```haskell
sum $ map sqrt [1..130]
```

If there wasn't the `$`, like in `sum map sqrt [1..130]`, this would have raised a compile-time error, since `sum` expects a list, and not a function returning a list. However, using `$` Haskell first evaluates `map sqrt [1..130]`, because `$` has lower precedence than space, and only after it applies the result as second argument of `$`. Another example can be `sqrt $ 3 + 4 + 9`, that computes the square root of `3 + 4 + 9`, unlike `sqrt 3 + 4 + 9`, which computes the square root of 3, and then adds 4 and 9.

`$` is also interesting because it allows us to pass function application as a function to higher-order functions:

```
ghci> map ($ 3) [(4+), (10*), (^2), sqrt]
[7.0,30.0,9.0,1.7320508075688772]
```

## Function composition
Function composition means that given two functions `f` and `g`, we want a third function `h` such as that, calling `h(x)` is equivalent to calling `f(g(x))`. Function composition in Haskell is done with the `.` operator:

```haskell
(.) :: (b -> c) -> (a -> b) -> a -> c
f . g = \x -> f (g x)
```

For example, `negate . (* 3)` is the composition of `negate` and `(* 3)`, so it's a function that takes a number, multiplies it by 3, and then negates the result.

In Haskell function composition is mainly used to write composite anonymous functions in a more concise way than using lambdas. For example, this lambda:

```haskell
map (\x -> negate (abs x)) [5,-3,-6,7,-3,2,-19,24]
```

becomes:

```haskell
map (negate . abs) [5,-3,-6,-7,-3,2,-19,24]
```

Or `map (\xs -> negate (sum (tail xs))) [[1..5],[3..6],[1..7]]` becomes `map (negate . sum . tail) [[1..5],[3..6],[1..7]]`.

If functions take more than one parameter, partial application comes in handy:

```haskell
sum (replicate 5 (max 6.7 8.9))
```

becomes

```haskell
sum . replicate 5 . max 6.7 $ 8.9
```

Here, function composition works with functions, so if we wrote `sum . replicate 5 . max 6.7 8.9` we would have been trying to compose `replicate 5`, which is a function (because it's partially applied), with `max 6.7 8.9`, which is not a function, because it already has all the arguments it needs to be evaluated. This would have produced a compilation error. What we must do instead is first create the final composed function, and then applying `8.9` to that. To be able to do that, we need the `.` operator to be used before the function application with `8.9` argument. Since the space has the highest precedence, we must need `$` to do function application last.

Since the dot operator is right-associative, `sum . replicate 5 . max 6.7 $ 8.9` works like this: first `max 6.7` is composed with `replicate 5`, to create a new function that takes one argument, calculates the max between it and `6.7`, and returns the list obtained replicating five times that maximum. Then, this new function is in turn composed to `sum`, to obtain a function that takes a number, creates the list of replicated maximums, and returns their sum. This big composite function is, last, applied to `8.9`.

Another usage for function composition is in combination with currying in function definition. For example:

```haskell
sum' :: (Num a) => [a] -> a
sum' xs = foldl (+) 0 xs
```

We know that we can write this as `sum' = foldl (+) 0`. Now, this is easy because there isn't function composition. What if we had:

```haskell
fn x = ceiling (negate (tan (cos (max 50 x))))
```

here we would like to get rid of the `x`, because it's used only once, and, in some way, it's the last parameter, so it would be nice to leverage partial application here. However, we can't just remove the `x`, otherwise we would end up with `cos (max 50)` which raises an error, because the cosine of a function is not defined. What we can do, instead, is:

```haskell
fn = ceiling . negate . tan . cos . max 50
```

And here we are returning a function which is the composition of all those functions, and we can omit the `x` because that function we are returning is partially applied.
