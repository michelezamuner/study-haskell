# 3. Types

- [Typeclasses](#typeclasses)
- [Common typeclasses](#common-typeclasses)

Every object in Haskell has a type, and every function must obey strict rules about the types of its arguments. In this way, the compiler can spot a great number of errors just checking if all values are used coherently with their declared types. Unlike other languages, such as C or Java, Haskell has *type inference*, which means that it's not necessary to declare the type of a value, because Haskell can *infer* it from the context.

To get the type of an expression in the interactive console, we use the `:t` command:

```
ghci> :t 'a'
'a' :: Char
ghci> :t True
True :: Bool
ghci> :t "HELLO!"
"HELLO!" :: [Char]
ghci> :t (True, 'a')
(True, 'a') :: (Bool, Char)
ghci> :t 4 == 5
4 == 5 :: Bool
```

Type names always have the first letter capitalized, and this is why function names can't begin with an uppercase letter. From the previous example, we can see how lists' types are defined only by the type of their elements, while tuples' types are defined both by the types of their elements (which can vary), and their number.

Although Haskell can infer the arguments type, and return type of a function from its definition, it's good practice to explicitly declare them:

```haskell
removeNonUppercase :: [Char] -> [Char]
removeNonUppercase st = [ c | c <- st, c `elem` ['A' .. 'Z'] ]
```

The syntax `[Char] -> [Char]` indicates a function that takes a `[Char]` and returns a `[Char]`. By the way, `[Char]` can also be written as `String`. The signature of a function that takes multiple parameter is as follows:

```haskell
addThree :: Int -> Int -> Int -> Int
addThree x y z = x + y + z
```

So, when we have a sequence of `<type> -> <type>`, the last type of the sequence is the return type, and all previous ones are the types of the arguments. In this example, we have a function taking three `Int` arguments, and returning another `Int`. Anyway, Haskell is always able to tell what the signature of a function is, just by looking at its definition. We can leverage this capability to tell the type of a function, using the `:t` command:

```
ghci> let removeNonUppercase st = [ c | c <- st, c `elem` ['A'..'Z'] ]
ghci> :t removeNonUppercase
removeNonUppercase :: [Char] -> [Char]
```

Here are some common types in Haskell:
- `Int`: bounded integer. On 32-bit machines the maximum value is 2^31-1 and the minimum is -2^31.
- `Integer`: unbounded integer. Can represent numbers bigger than `Int`, but it's less efficient.
- `Float`: floating point number with single precision.
- `Double`: floating point number with double precision.
- `Bool`: boolean type. It only has the values `True` and `False`.
- `Char`: single character, denoted by single quotes.

Sometimes, some functions can't be described only by using type names. For example, the `head` function takes a list, and returns its first element. How could we write its signature? Since this function works with lists of any type, we can't write something like `[Char] -> Char`, because this function works also with `Int`, etc. We need a way to indicate a value of a generic type. This is accomplished by *type variables*, which are variable names representing any type:

```
ghci> :t head
head :: [a] -> a
```

Here, `a` stands for any type, stating that this function can work as `[Char] -> Char`, `[Int] -> Int`, and so on for every possible type. Functions with type variables are called *polymorphic functions*, because their actual behavior depends on the actual type of the values with which they are used.

## Typeclasses
A *Typeclass* in Haskell is an interface, defining what functions or operators must be defined for a collection of types:

```
ghci> :t (==)
(==) :: (Eq a) => a -> a -> Bool
```

Here `(Eq a)` is called *class constraint*, and represents the interface of types used next in the signature. In this case, the `(==)` function takes two values `a` of any type, and returns a `Bool`, but the type of `a` should be instances of the `Eq` class (in Java we would say that they should implement the `Eq` interface).

The `Eq` typeclass represents all types that it makes sense to test for equality, and in Haskell this comprises all types except for `IO` and functions. For example, the `elem` function has type of `(Eq a) => a -> [a] -> Bool`, so it takes a value, a list of values of the same type, and returns if the value is comprised in the list: but, since to perform this check it's needed to check if the value is equal to another value in the list, these values must support equality tests, and so their type must instantiate the `Eq` class.

## Common typeclasses
- `Eq` comprises types that support equality testing. This typeclass states that all its types must implement the `==` and `/=` functions.
- `Ord` comprises types that support ordering of their elements, and they must implement such functions as `>`, `<`, `<=` and so on.
- `Show` comprises types that can be represented as strings. This is used for example by the `show` function that takes a value and returns its string representation. The type of `show` is `Show a => a -> String`, meaning that it takes a value of a type implementing `Show`, and returns a `String`.

```
ghci> show 3
"3"
ghci> show 5.334
"5.334"
ghci> show True
"True"
```

- `Read` comprises types that can be constructed from string. The `read` function has type `Read a => String -> a`, it takes a `String` and returns a value of a "`Read`-able" type.

```
ghci> read "True" || False
True
ghci> read "8.2" + 3.8
12.0
ghci> read "[1,2,3,4]" ++ [3]
[1,2,3,4,3]
```

However, be careful that `read` isn't able to tell what `Read`-able type to produce, unless it's given a context allowing it to decide. For example, calling `read "3"` doesn't make much sense, because `3` could be a `Int` or a `Float`; or `read "a"` could be a `Char` or a `String`. For this reason, trying to call `read` alone results in an error being thrown.

Another way to fix this issue, other than using the return of `read` inside a context, is using *type annotations*:

```
ghci> read "5" :: Int
5
ghci> read "[1,2,3,4]" :: [Float]
[1.0,2.0,3.0,4.0]
```

In general, type annotations can be used to tell the compiler which type we want an expression to be of, in those circumstances where it can't figure it out by itself.

- `Enum` comprises types that support sequential ordering of their elements. The main difference between `Ord` types and `Enum` types is that with `Enum`s there's a way to build ranges, the `succ` and `pred` functions can be used to find successors and predecessors.

- `Bounded` comprises types whose elements have upper and lower bound, and features the `minBound` and `maxBound` functions. These functions don't take any argument, and so they can be though as being constants:

```
ghci>:t minBound
minBound :: (Bounded a) => a
```

However, like before if we called `minBound` on its own, the compiler here wouldn't have any way to tell which actual type should the minimum bound be, so we either use it inside a context, or apply a type annotation:

```
ghci>minBound :: Char
'\NUL'
```

For this reason, these are known as *polymorphic constants*, because their actual value depends on the actual type used.

- `Num` comprises numeric types, featuring all common operations on numbers, such as `*`, `+`, etc. Here it's interesting to note that the actual numbers we use inside operations, like `1`, `20`, and so on, are again polymorphic constants of typeclass `Num`:

```
ghci>:t 1
1 :: (Num a) => a
```

and so to use a number on its own, we must specify of what actual type we want that number to be:

```
ghci>1 :: Int
1
ghci>1 :: Float
1.0
```

- `Integral` comprises integral types, like `Int` and `Integer`.
- `Floating` comprises types representing floating point numbers, like `Float` and `Double`.

`fromIntegral` is a handy function useful to convert an integral number into a more generic number:

```haskell
fromIntegral (length [1,2,3,4]) + 3.2
```

its type is `(Num b, Integral a) => a -> b` and it's an example of a class constraint applied to two types, aptly named `a` and `b`.
