# Haskell notes

Haskell notes from studying [learn you a haskell](http://learnyouahaskell.com).

* [1. Basics](1-basics.md)
* [2. Lists and tuples](2-lists-and-tuples.md)
* [3. Types](3-types.md)
* [4. Functions](4-functions.md)
* [5. Modules](5-modules.md)
* [6. Custom types and typeclasses](6-custom-types-and-typeclasses.md)
* [7. Functors and monoids](7-functors-and-monoids.md)
* [8. Monads](8-monads.md)
* [9. Input and output](9-input-and-output.md)